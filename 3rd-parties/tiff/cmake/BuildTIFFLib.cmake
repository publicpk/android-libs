
cmake_minimum_required (VERSION 3.0)

include(ExternalProject)

# For the information about Version and Download
# see http://www.simplesystems.org/libtiff/

set(LIB_VERSION "4.0.8")
set(LIB_NAME "tiff-${LIB_VERSION}")
set(LIB_URL "ftp://download.osgeo.org/libtiff/tiff-${LIB_VERSION}.tar.gz")
set(LIB_MD5 "0")

set(LIB_BASE_DIR "${CMAKE_CURRENT_BINARY_DIR}/3rd-parties")
set(LIB_INST_DIR "${LIB_BASE_DIR}/installed")

message(">>> Building ${LIB_NAME}")
message("   - URL: ${LIB_URL}")

ExternalProject_Add(
    ${LIB_NAME}
    URL ${LIB_URL}
    DOWNLOAD_DIR ${LIB_BASE_DIR}
    PREFIX ${LIB_BASE_DIR}
    SOURCE_DIR ${LIB_BASE_DIR}/src/${LIB_NAME}
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND ./configure --prefix=${LIB_INST_DIR}
    BUILD_COMMAND make
    INSTALL_COMMAND make install
)
