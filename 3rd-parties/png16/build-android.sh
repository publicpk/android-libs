#!/bin/bash

BUILD_CONFIGS=(
    Release
#    Debug
)

ABIS=(
    armeabi
    # armeabi-v7a
    # arm64-v8a
    # x86
    # x86_64
    # mips
    # mips64
)

cmake=/home/peterk/.local/android/sdk/cmake/3.6.3155560/bin/cmake
ninja=/home/peterk/.local/android/sdk/cmake/3.6.3155560/bin/ninja
ndk_dir=/home/peterk/.local/android/sdk/ndk-bundle

function generate_build_script() {
    local build_config="$1"
    local abi="$2"
    $cmake \
        -H../../.. \
        -B. \
        -G"Android Gradle - Ninja" \
        -DANDROID_ABI=$abi \
        -DANDROID_NDK=$ndk_dir \
        -DCMAKE_LIBRARY_OUTPUT_DIRECTORY=./intermediates/cmake/obj \
        -DCMAKE_BUILD_TYPE=$build_config \
        -DCMAKE_MAKE_PROGRAM=$ninja \
        -DCMAKE_TOOLCHAIN_FILE=$ndk_dir/build/cmake/android.toolchain.cmake \
        -DANDROID_PLATFORM=android-16 \
        -DCMAKE_C_FLAGS="-fexceptions -frtti" \
        -DCMAKE_CXX_FLAGS="-frtti -fexceptions" \
        -D__STDC_FORMAT_MACROS=1 \
        -DANDROID_TOOLCHAIN=clang
}

mkdir -p ./build-android
pushd ./build-android

for config in "${BUILD_CONFIGS[@]}"; do
    mkdir -p ${config}
    pushd ${config}
    for abi in "${ABIS[@]}"; do
        mkdir -p ${abi}
        pushd ${abi}

        # generate build scripts
        generate_build_script $config $abi

        # build
        $cmake --build .

        popd
    done
    popd
done

popd

